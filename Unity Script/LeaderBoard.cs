using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;
using System;


public static class JsonHelper
{
	public static string fixJson(string value)
	{
    	value = "{\"Items\":" + value + "}";
    	return value;
	}
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}


[Serializable]
public class LB_Player : IComparable{
	public string _id;
	public string name;
	public int score;

	 public int CompareTo(object obj) {
     LB_Player otherScore = obj as LB_Player;
     if(otherScore!=null) {
        return this.score.CompareTo(otherScore.score);
     } else {
        throw new ArgumentException("Object is not a Score");
     }
  }
}


public class LeaderBoard : MonoBehaviour {
	
	[SerializeField] private List<LB_Player> leaderboard = new List<LB_Player>();
    private string api_url = "your-api-url";


	public void GetScores() {
        IEnumerator coroutine = GetRequest(boardURL);
		StartCoroutine(coroutine);
	}
	
	IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            if (webRequest.isNetworkError)
            {
                Debug.Log(pages[page] + ": Error: " + webRequest.error);
            }
            else
            {
				string json = webRequest.downloadHandler.text;

				json = JsonHelper.fixJson(json);

				LB_Player[] playerArray = JsonHelper.FromJson<LB_Player>(json);
				
				//Player scores are now stored into the list
				leaderboard = new List<LB_Player>(playerArray);
				
            }
        }
    }


    public void UploadScores(string name, string score){
        IEnumerator coroutine = UploadScore(name,score);
        StartCoroutine(coroutine);
    }

    private IEnumerator UploadScore(string name, string score)
	{
		WWWForm form = new WWWForm();
		form.AddField("name", name);
		form.AddField("score", score);

		using (UnityWebRequest www = UnityWebRequest.Post(boardURL, form))
		{
			yield return www.SendWebRequest();

			if (www.isNetworkError || www.isHttpError)
			{
				Debug.Log(www.error);
			}
			else
			{
				Debug.Log("HighScore sent to Data Base");
			}
		}
	}

}
